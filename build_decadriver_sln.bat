
REM @ECHO OFF
IF NOT EXIST "build\" MKDIR build
CD build
CMAKE -G "Visual Studio 15 2017" ..
IF ERRORLEVEL 1 GOTO ERROR
START /B dw3xxx.sln
EXIT
:ERROR
POPD
PAUSE
